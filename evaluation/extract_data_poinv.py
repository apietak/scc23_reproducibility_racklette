import sys
import os
import re

if len(sys.argv) != 4:
    print("Usage: ")
    print("python3 extract_data.py <folder> <out_filename> <filename_beginning>")
    exit(0)

folder = sys.argv[1]
file_beginning = sys.argv[3]


files = []
for file in os.listdir(folder):
    if file.startswith(file_beginning) and not file.endswith(".log"):
        files.append(file)

out_file = open(folder + "/" + sys.argv[2], "w")
out_file.write("Id Function     threads gpus  P  Q mtxfmt  nb uplo        n   lda       seedA          tsub          time        gflops comm\n")
for file_name in files:
    file = open(file_name, "r")
    lines = file.readlines()
    for line in lines:
        line = line.strip()
        if line.startswith("0 dpoinv"):
            out_file.write(line + " ")
    file.seek(0)
    content = file.read()
    com_amount = 0.
    pattern = re.compile(r"\[starpu_comm_stats\]\[\d\] TOTAL:.(?P<b>\d*.\d*)\D*(?P<mb>\d*.\d*)\D*(?P<bs>\d*.\d*)\D*(?P<mbs>\d*.\d*)\D*")
    for match in pattern.finditer(content):
        com_amount = com_amount + float(match.group(2))
    out_file.write(str(com_amount)+"\n")
    
