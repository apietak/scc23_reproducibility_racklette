library(ggplot2)
library(ggthemes)
library(data.table)
library(Hmisc)
theme_set(theme_bw(base_size = 10))
o_color <- "#619cff"
o_shape <- 16

df <- read.table("blocks.txt", header=TRUE, sep="")


p1 <- ggplot() +
    stat_summary(df, mapping=aes(nb,gflops), fill=o_color, fun.data=mean_cl_boot, geom="ribbon", alpha=0.15) +
    stat_summary(df, mapping=aes(nb,gflops), color=o_color, fun.y=mean, geom="line", linewidth=0.75) +
    stat_summary(df, mapping=aes(nb,gflops), color=o_color, shape=o_shape, fun.y=mean, geom="point", size=1.75) +
    scale_x_continuous(breaks=seq(100,1000,200), labels = paste(as.character(seq(100,1000,200)), "²", sep="")) +
    scale_y_continuous(breaks = seq(0,2500,250)) +
    xlab("Block Size [elements]") +
    ylab("GFlop/s") +
    ggtitle("GFlop/s vs Block Size")
p1

p2 <- ggplot()+
    stat_summary(df, mapping=aes(nb,time), fill=o_color, fun.data=mean_cl_boot, geom="ribbon", alpha=0.15) +
    stat_summary(df, mapping=aes(nb,time), color=o_color, fun.y=mean, geom="line", linewidth=0.75) +
    stat_summary(df, mapping=aes(nb,time), color=o_color, shape=o_shape, fun.y=mean, geom="point", size=1.75) +
    scale_x_continuous(breaks=seq(100,1000,200), labels = paste(as.character(seq(100,1000,200)), "²", sep="")) +
    scale_y_continuous(breaks=seq(0,200,20))+
    xlab("Block Size [elements]") +
    ylab("Time [s]") +
    ggtitle("Time vs Block Size")
p2

ggsave("nb_vs_gflops.png", p1,  width=100, height=60,units = "mm")
ggsave("nb_vs_time.png", p2,  width=100, height=60,units = "mm")


