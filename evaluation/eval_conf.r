library(ggplot2)
library(ggthemes)
library(data.table)
library(Hmisc)
theme_set(theme_bw(base_size = 10))

o <- read.table("ompi.txt", header=TRUE, sep="")
o <- o[order(o$n),]
o_s <- read.table("ompi_sbc.txt", header=TRUE, sep="")
o_s <- o_s[order(o_s$n),]
library(ggplot2)
library(ggthemes)
library(data.table)
library(Hmisc)
library(plyr)
theme_set(theme_bw(base_size = 10))

o <- read.table("ompi.txt", header=TRUE, sep="")
o <- o[order(o$n),]
o_s <- read.table("ompi_sbc.txt", header=TRUE, sep="")
o_s <- o_s[order(o_s$n),]

c <- read.table("mich.txt", header=TRUE, sep="")
c <- c[order(c$n),]
c_s <- read.table("mich_sbc.txt", header=TRUE, sep="")
c_s <- c_s[order(c_s$n),]

i <- read.table("intel.txt", header=TRUE, sep="")
i <- i[order(i$n),]
i_s <- read.table("intel_sbc.txt", header=TRUE, sep="")
i_s <- i_s[order(i_s$n),]

o$comm = o$comm/1000
o_s$comm = o_s$comm/1000
c$comm = c$comm/1000
c_s$comm = c_s$comm/1000
i$comm = i$comm/1000
i_s$comm = i_s$comm/1000


fto <- nls(time ~ a*(n**b), data=o, start=list(a=1, b=3))
fto_s <- nls(time ~ a*(n**b), data=o_s, start=list(a=1, b=3))

ftc <- nls(time ~ a*(n**b), data=c, start=list(a=1, b=3))
ftc_s <- nls(time ~ a*(n**b), data=c_s, start=list(a=1, b=3))

fti <- nls(time ~ a*(n**b), data=i, start=list(a=1, b=3))
fti_s <- nls(time ~ a*(n**b), data=i_s, start=list(a=1, b=3))

fgo <- nls(gflops ~ a*(n**b), data=o, start=list(a=10000, b=0.02))
fgo_s <- nls(gflops ~ a*(n**b), data=o_s, start=list(a=1, b=0.5))

fgc <- nls(gflops ~ a*(n**b), data=c, start=list(a=10000, b=0.02))
fgc_s <- nls(gflops ~ a*(n**b), data=c_s, start=list(a=1, b=0.5))

fgi <- nls(gflops ~ a*(n**b), data=i, start=list(a=10000, b=0.02))
fgi_s <- nls(gflops ~ a*(n**b), data=i_s, start=list(a=1, b=0.5))



p1 <- ggplot() +
    stat_summary(o, mapping=aes(n,gflops, fill="Open MPI", linetype="2DBC", group = "Open MPI"), fun.data=mean_cl_boot, geom="ribbon", alpha=0.25, show.legend=FALSE) +
    stat_summary(o, mapping=aes(n,gflops, color="Open MPI", linetype="2DBC", group = "Open MPI"), fun.y=mean, geom="line", linewidth=0.75) +
    stat_summary(o, mapping=aes(n,gflops, color="Open MPI", shape="Open MPI", group = "Open MPI"), fun.y=mean, geom="point", size=1.75) +
    stat_summary(o_s, mapping=aes(n,gflops, fill="Open MPI", linetype="SBC", group = "Open MPI"), fun.data=mean_cl_boot, geom="ribbon", alpha=0.25, show.legend=FALSE) +
    stat_summary(o_s, mapping=aes(n,gflops, color="Open MPI", linetype="SBC", group = "Open MPI"), fun.y=mean, geom="line",linewidth=0.75) +
    stat_summary(o_s, mapping=aes(n,gflops, color="Open MPI", shape="Open MPI", group = "Open MPI"), fun.y=mean, geom="point") +
    
    stat_summary(c, mapping=aes(n,gflops, fill="MPICH", linetype="2DBC", group = "MPICH"), fun.data=mean_cl_boot, geom="ribbon", alpha=0.25, show.legend=FALSE) +
    stat_summary(c, mapping=aes(n,gflops, color="MPICH", linetype="2DBC", group = "MPICH"), fun.y=mean, geom="line",linewidth=0.75) +
    stat_summary(c, mapping=aes(n,gflops, color="MPICH", shape="MPICH", group = "MPICH"), fun.y=mean, geom="point", size=1.75) +
    stat_summary(c_s, mapping=aes(n,gflops, fill="MPICH", linetype="SBC", group = "MPICH"), fun.data=mean_cl_boot, geom="ribbon", alpha=0.25, show.legend=FALSE) +
    stat_summary(c_s, mapping=aes(n,gflops, color="MPICH", linetype="SBC", group = "MPICH"), fun.y=mean, geom="line",linewidth=0.75) +
    stat_summary(c_s, mapping=aes(n,gflops, color="MPICH", shape="MPICH", group = "MPICH"), fun.y=mean, geom="point", size=1.75) +
    
    stat_summary(i, mapping=aes(n,gflops, fill="Intel MPI", linetype="2DBC", group = "Intel MPI"), fun.data=mean_cl_boot, geom="ribbon", alpha=0.25, show.legend=FALSE) +
    stat_summary(i, mapping=aes(n,gflops, color="Intel MPI", linetype="2DBC", group = "Intel MPI"), fun.y=mean, geom="line",linewidth=0.75) +
    stat_summary(i, mapping=aes(n,gflops, color="Intel MPI", shape="Intel MPI", group = "Intel MPI"), fun.y=mean, geom="point", size=1.75) +
    stat_summary(i_s, mapping=aes(n,gflops, fill="Intel MPI", linetype="SBC", group = "Intel MPI"), fun.data=mean_cl_boot, geom="ribbon", alpha=0.25, show.legend=FALSE) +
    stat_summary(i_s, mapping=aes(n,gflops, color="Intel MPI", linetype="SBC", group = "Intel MPI"), fun.y=mean, geom="line",linewidth=0.75) +
    stat_summary(i_s, mapping=aes(n,gflops, color="Intel MPI", shape="Intel MPI", group = "Intel MPI"), fun.y=mean, geom="point", size=1.75) +
    scale_y_continuous(breaks = seq(0,11000,1000)) +
    scale_x_continuous(breaks = 500*seq(50,250,50), labels = paste(as.character(500*seq(50,250,50)/1000), "k²", sep="")) +
    theme(legend.position = "bottom", legend.box="vertical", legend.margin = margin(-10,0,0,0), legend.spacing.y = unit(5,"pt"), axis.text.x = element_text(margin=margin(4,0,0,0), angle=0), plot.margin = margin(10, 25, 10, 10), axis.title.x = element_text(margin = margin(5,10,10,10))) +
    labs(color = "MPI", linetype = "Distribution") +
    ylab("GFlop/s") +
    xlab("Matrix Size [elements]")+
    ggtitle("GFlop/s vs Matrix Size") +
    guides(colour = guide_legend(override.aes = list(shape = c(16,17,15)))) +
    scale_shape(guide = FALSE)
p1

p2 <- ggplot()+
    stat_summary(o, mapping=aes(n,time, fill="Open MPI", linetype="2DBC"), fun.data=mean_cl_boot, geom="ribbon", alpha=0.25, show.legend=FALSE) +
    stat_summary(o, mapping=aes(n,time, color="Open MPI", linetype="2DBC"), fun.y=mean, geom="line", linewidth=0.75) +
    stat_summary(o, mapping=aes(n,time, color="Open MPI", shape="Open MPI"), fun.y=mean, geom="point", size=1.75) +
    stat_summary(o_s, mapping=aes(n,time, fill="Open MPI", linetype="SBC"), fun.data=mean_cl_boot, geom="ribbon", alpha=0.25, show.legend=FALSE) +
    stat_summary(o_s, mapping=aes(n,time, color="Open MPI", linetype="SBC"), fun.y=mean, geom="line", linewidth=0.75) +
    stat_summary(o_s, mapping=aes(n,time, color="Open MPI", shape="Open MPI"), fun.y=mean, geom="point", size=1.75) +
    
    stat_summary(c, mapping=aes(n,time, fill="MPICH", linetype="2DBC"), fun.data=mean_cl_boot, geom="ribbon", alpha=0.25, show.legend=FALSE) +
    stat_summary(c, mapping=aes(n,time, color="MPICH", linetype="2DBC"), fun.y=mean, geom="line", linewidth=0.75) +
    stat_summary(c, mapping=aes(n,time, color="MPICH", shape="MPICH"), fun.y=mean, geom="point", size=1.75) +
    stat_summary(c_s, mapping=aes(n,time, fill="MPICH", linetype="SBC"), fun.data=mean_cl_boot, geom="ribbon", alpha=0.25, show.legend=FALSE) +
    stat_summary(c_s, mapping=aes(n,time, color="MPICH", linetype="SBC"), fun.y=mean, geom="line", linewidth=0.75) +
    stat_summary(c_s, mapping=aes(n,time, color="MPICH", shape="MPICH"), fun.y=mean, geom="point", size=1.75) +
    
    stat_summary(i, mapping=aes(n,time, fill="Intel MPI", linetype="2DBC"), fun.data=mean_cl_boot, geom="ribbon", alpha=0.25, show.legend=FALSE) +
    stat_summary(i, mapping=aes(n,time, color="Intel MPI", linetype="2DBC"), fun.y=mean, geom="line", linewidth=0.75) +
    stat_summary(i, mapping=aes(n,time, color="Intel MPI", shape="Intel MPI"), fun.y=mean, geom="point", size=1.75) +
    stat_summary(i_s, mapping=aes(n,time, fill="Intel MPI", linetype="SBC"), fun.data=mean_cl_boot, geom="ribbon", alpha=0.25, show.legend=FALSE) +
    stat_summary(i_s, mapping=aes(n,time, color="Intel MPI", linetype="SBC"), fun.y=mean, geom="line", linewidth=0.75) +
    stat_summary(i_s, mapping=aes(n,time, color="Intel MPI", shape="Intel MPI"), fun.y=mean, geom="point", size=1.75) +
    scale_y_continuous(breaks = seq(0,120,10)) +
     scale_x_continuous(breaks = 500*seq(50,250,50), labels = paste(as.character(500*seq(50,250,50)/1000), "k²", sep="")) +
    theme(legend.position = "bottom", legend.box="vertical", legend.margin = margin(-10,0,0,0), legend.spacing.y = unit(5,"pt"), axis.text.x = element_text(margin=margin(4,0,0,0), angle=0), plot.margin = margin(10, 25, 10, 10), axis.title.x = element_text(margin = margin(5,10,10,10))) +
    labs(color = "MPI", linetype = "Distribution") +
    ylab("Time [s]") +
    xlab("Matrix Size [elements]")+
    ggtitle("Time vs Matrix Size")+
    guides(colour = guide_legend(override.aes = list(shape = c(16,17,15)))) +
    scale_shape(guide = FALSE)
p2

p3 <- ggplot()+
    stat_summary(o, mapping=aes(n,comm, fill="Open MPI", linetype="2DBC"), fun.data=mean_cl_boot, geom="ribbon", alpha=0.25, show.legend=FALSE) +
    stat_summary(o, mapping=aes(n,comm, color="Open MPI", linetype="2DBC"), fun.y=mean, geom="line", linewidth=0.75) +
    stat_summary(o, mapping=aes(n,comm, color="Open MPI", shape="Open MPI"), fun.y=mean, geom="point", size=1.75) +
    stat_summary(o_s, mapping=aes(n,comm, fill="Open MPI", linetype="SBC"), fun.data=mean_cl_boot, geom="ribbon", alpha=0.25, show.legend=FALSE) +
    stat_summary(o_s, mapping=aes(n,comm, color="Open MPI", linetype="SBC"), fun.y=mean, geom="line", linewidth=0.75) +
    stat_summary(o_s, mapping=aes(n,comm, color="Open MPI", shape="Open MPI"), fun.y=mean, geom="point", size=1.75) +
    
    stat_summary(c, mapping=aes(n,comm, fill="MPICH", linetype="2DBC"), fun.data=mean_cl_boot, geom="ribbon", alpha=0.25, show.legend=FALSE) +
    stat_summary(c, mapping=aes(n,comm, color="MPICH", linetype="2DBC"), fun.y=mean, geom="line", linewidth=0.75) +
    stat_summary(c, mapping=aes(n,comm, color="MPICH", shape="MPICH"), fun.y=mean, geom="point", size=1.75) +
    stat_summary(c_s, mapping=aes(n,comm, fill="MPICH", linetype="SBC"), fun.data=mean_cl_boot, geom="ribbon", alpha=0.25, show.legend=FALSE) +
    stat_summary(c_s, mapping=aes(n,comm, color="MPICH", linetype="SBC"), fun.y=mean, geom="line", linewidth=0.75) +
    stat_summary(c_s, mapping=aes(n,comm, color="MPICH", shape="MPICH"), fun.y=mean, geom="point", size=1.75) +
    
    stat_summary(i, mapping=aes(n,comm, fill="Intel MPI", linetype="2DBC"), fun.data=mean_cl_boot, geom="ribbon", alpha=0.25, show.legend=FALSE) +
    stat_summary(i, mapping=aes(n,comm, color="Intel MPI", linetype="2DBC"), fun.y=mean, geom="line", linewidth=0.75) +
    stat_summary(i, mapping=aes(n,comm, color="Intel MPI", shape="Intel MPI"), fun.y=mean, geom="point", size=1.75) +
    stat_summary(i_s, mapping=aes(n,comm, fill="Intel MPI", linetype="SBC"), fun.data=mean_cl_boot, geom="ribbon", alpha=0.25, show.legend=FALSE) +
    stat_summary(i_s, mapping=aes(n,comm, color="Intel MPI", linetype="SBC"), fun.y=mean, geom="line", linewidth=0.75) +
    stat_summary(i_s, mapping=aes(n,comm, color="Intel MPI", shape="Intel MPI"), fun.y=mean, geom="point", size=1.75) +
    scale_y_continuous(breaks = seq(0,1e6,1e5)/1000) +
     scale_x_continuous(breaks = 500*seq(50,250,50), labels = paste(as.character(500*seq(50,250,50)/1000), "k²", sep="")) +
    theme(legend.position = "bottom", legend.box="vertical", legend.margin = margin(-10,0,0,0), legend.spacing.y = unit(5,"pt"), axis.text.x = element_text(margin=margin(4,0,0,0), angle=0), plot.margin = margin(10, 25, 10, 10), axis.title.x = element_text(margin = margin(5,10,10,10))) +
    labs(color = "MPI", linetype = "Distribution") +
    ylab("Communication Volume [GB]") +
    xlab("Matrix Size [elements]")+
    ggtitle("Com. Volume vs Matrix Size")+
    guides(colour = guide_legend(override.aes = list(shape = c(16,17,15)))) +
    scale_shape(guide = FALSE)
p3

ggsave("n_vs_gflops.png", p1, width=100, height=80,units = "mm")
ggsave("n_vs_time.png", p2,  width=100, height=80,units = "mm")
ggsave("n_vs_com.png", p3,  width=100, height=80,units = "mm")




c <- read.table("ch.txt", header=TRUE, sep="")
c <- c[order(c$n),]
c_s <- read.table("ch_sbc.txt", header=TRUE, sep="")
c_s <- c_s[order(c_s$n),]

i <- read.table("intel.txt", header=TRUE, sep="")
i <- i[order(i$n),]
i_s <- read.table("intel_sbc.txt", header=TRUE, sep="")
i_s <- i_s[order(i_s$n),]


o_g <- o_s$gflops/o$gflops
c_g <- c_s$gflops/c$gflops
i_g <- i_s$gflops/i$gflops

mean(i_g)
var(i_g)
mean(o_g)
var(o_g)
mean(c_g)
var(c_g)

o_t <- o$time/o_s$time
c_t <- c$time/c_s$time
i_t <- i$time/i_s$time

mean(i_t)
var(i_t)
mean(o_t)
var(o_t)
mean(c_t)
var(c_t)



p1 <- ggplot() +
    stat_summary(o, mapping=aes(n,gflops, fill="Open MPI", linetype="2DBC"), fun.data=mean_cl_boot, geom="ribbon", alpha=0.15, show.legend=FALSE) +
    stat_summary(o, mapping=aes(n,gflops, color="Open MPI", linetype="2DBC"), fun.y=mean, geom="line") +
    stat_summary(o_s, mapping=aes(n,gflops, fill="Open MPI", linetype="SBC"), fun.data=mean_cl_boot, geom="ribbon", alpha=0.15, show.legend=FALSE) +
    stat_summary(o_s, mapping=aes(n,gflops, color="Open MPI", linetype="SBC"), fun.y=mean, geom="line") +
    
    stat_summary(c, mapping=aes(n,gflops, fill="MPICH", linetype="2DBC"), fun.data=mean_cl_boot, geom="ribbon", alpha=0.15, show.legend=FALSE) +
    stat_summary(c, mapping=aes(n,gflops, color="MPICH", linetype="2DBC"), fun.y=mean, geom="line") +
    stat_summary(c_s, mapping=aes(n,gflops, fill="MPICH", linetype="SBC"), fun.data=mean_cl_boot, geom="ribbon", alpha=0.15, show.legend=FALSE) +
    stat_summary(c_s, mapping=aes(n,gflops, color="MPICH", linetype="SBC"), fun.y=mean, geom="line") +
    
    stat_summary(i, mapping=aes(n,gflops, fill="Intel MPI", linetype="2DBC"), fun.data=mean_cl_boot, geom="ribbon", alpha=0.15, show.legend=FALSE) +
    stat_summary(i, mapping=aes(n,gflops, color="Intel MPI", linetype="2DBC"), fun.y=mean, geom="line") +
    stat_summary(i_s, mapping=aes(n,gflops, fill="Intel MPI", linetype="SBC"), fun.data=mean_cl_boot, geom="ribbon", alpha=0.15, show.legend=FALSE) +
    stat_summary(i_s, mapping=aes(n,gflops, color="Intel MPI", linetype="SBC"), fun.y=mean, geom="line") +
    scale_y_continuous(breaks = seq(0,11000,1000)) +
    scale_x_continuous(breaks = 500*seq(25,275,50)) +
    theme(legend.position = "bottom", legend.box="vertical", legend.margin = margin(0,0,0,0), legend.spacing.y = unit(-2,"pt")) +
    labs(color = "MPI", linetype = "Distribution") +
    ylab("Gflops") +
    xlab("Matrix Size")+
    ggtitle("Matrix Size vs Gflops")
p1

p2 <- ggplot()+
    stat_summary(o, mapping=aes(n,time, fill="Open MPI", linetype="2DBC"), fun.data=mean_cl_boot, geom="ribbon", alpha=0.15, show.legend=FALSE) +
    stat_summary(o, mapping=aes(n,time, color="Open MPI", linetype="2DBC"), fun.y=mean, geom="line") +
    stat_summary(o_s, mapping=aes(n,time, fill="Open MPI", linetype="SBC"), fun.data=mean_cl_boot, geom="ribbon", alpha=0.15, show.legend=FALSE) +
    stat_summary(o_s, mapping=aes(n,time, color="Open MPI", linetype="SBC"), fun.y=mean, geom="line") +
    
    stat_summary(c, mapping=aes(n,time, fill="MPICH", linetype="2DBC"), fun.data=mean_cl_boot, geom="ribbon", alpha=0.15, show.legend=FALSE) +
    stat_summary(c, mapping=aes(n,time, color="MPICH", linetype="2DBC"), fun.y=mean, geom="line") +
    stat_summary(c_s, mapping=aes(n,time, fill="MPICH", linetype="SBC"), fun.data=mean_cl_boot, geom="ribbon", alpha=0.15, show.legend=FALSE) +
    stat_summary(c_s, mapping=aes(n,time, color="MPICH", linetype="SBC"), fun.y=mean, geom="line") +
    
    stat_summary(i, mapping=aes(n,time, fill="Intel MPI", linetype="2DBC"), fun.data=mean_cl_boot, geom="ribbon", alpha=0.15, show.legend=FALSE) +
    stat_summary(i, mapping=aes(n,time, color="Intel MPI", linetype="2DBC"), fun.y=mean, geom="line") +
    stat_summary(i_s, mapping=aes(n,time, fill="Intel MPI", linetype="SBC"), fun.data=mean_cl_boot, geom="ribbon", alpha=0.15, show.legend=FALSE) +
    stat_summary(i_s, mapping=aes(n,time, color="Intel MPI", linetype="SBC"), fun.y=mean, geom="line") +
    scale_y_continuous(breaks = seq(0,120,10)) +
    scale_x_continuous(breaks = 500*seq(25,275,50)) +
    theme(legend.position = "bottom", legend.box="vertical", legend.margin = margin(0,0,0,0), legend.spacing.y = unit(-2,"pt")) +
    labs(color = "MPI", linetype = "Distribution") +
    ylab("Time [s]") +
    xlab("Matrix Size")+
    ggtitle("Matrix Size vs Time")
p2

p3 <- ggplot()+
    stat_summary(o, mapping=aes(n,comm, fill="Open MPI", linetype="2DBC"), fun.data=mean_cl_boot, geom="ribbon", alpha=0.15, show.legend=FALSE) +
    stat_summary(o, mapping=aes(n,comm, color="Open MPI", linetype="2DBC"), fun.y=mean, geom="line") +
    stat_summary(o_s, mapping=aes(n,comm, fill="Open MPI", linetype="SBC"), fun.data=mean_cl_boot, geom="ribbon", alpha=0.15, show.legend=FALSE) +
    stat_summary(o_s, mapping=aes(n,comm, color="Open MPI", linetype="SBC"), fun.y=mean, geom="line") +
    
    stat_summary(c, mapping=aes(n,comm, fill="MPICH", linetype="2DBC"), fun.data=mean_cl_boot, geom="ribbon", alpha=0.15, show.legend=FALSE) +
    stat_summary(c, mapping=aes(n,comm, color="MPICH", linetype="2DBC"), fun.y=mean, geom="line") +
    stat_summary(c_s, mapping=aes(n,comm, fill="MPICH", linetype="SBC"), fun.data=mean_cl_boot, geom="ribbon", alpha=0.15, show.legend=FALSE) +
    stat_summary(c_s, mapping=aes(n,comm, color="MPICH", linetype="SBC"), fun.y=mean, geom="line") +
    
    stat_summary(i, mapping=aes(n,comm, fill="Intel MPI", linetype="2DBC"), fun.data=mean_cl_boot, geom="ribbon", alpha=0.15, show.legend=FALSE) +
    stat_summary(i, mapping=aes(n,comm, color="Intel MPI", linetype="2DBC"), fun.y=mean, geom="line") +
    stat_summary(i_s, mapping=aes(n,comm, fill="Intel MPI", linetype="SBC"), fun.data=mean_cl_boot, geom="ribbon", alpha=0.15, show.legend=FALSE) +
    stat_summary(i_s, mapping=aes(n,comm, color="Intel MPI", linetype="SBC"), fun.y=mean, geom="line") +
    scale_y_continuous(breaks = seq(0,1e6,1e5)) +
    scale_x_continuous(breaks = 500*seq(25,275,50)) +
    theme(legend.position = "bottom", legend.box="vertical", legend.margin = margin(0,0,0,0), legend.spacing.y = unit(-2,"pt")) +
    labs(color = "MPI", linetype = "Distribution") +
    ylab("Communications [MB]") +
    xlab("Matrix Size")+
    ggtitle("Matrix Size vs Communications")
p3

ggsave("n_vs_gflops.png", p1, width=100, height=80,units = "mm")
ggsave("n_vs_time.png", p2,  width=100, height=80,units = "mm")
ggsave("n_vs_com.png", p3,  width=100, height=80,units = "mm")



