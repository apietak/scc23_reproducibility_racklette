library(ggplot2)
library(ggthemes)
theme_set(theme_bw(base_size=10))

o_color <- "#619cff"
c_color <- "#00ba38"
i_color <- "#f8766d"

d <- read.table("power_consumption.log", sep="", blank.lines.skip=TRUE, fill=TRUE)
d <- d[complete.cases(d),]
colnames(d) <- c("time", "factor", "active", "apparent")


os <- as.integer(as.POSIXct("2023-11-14 02:16:04 MST"), tz = "MST") -10
oe <- as.integer(as.POSIXct("2023-11-14 02:43:21 MST"), tz = "MST") +10

is <- as.integer(as.POSIXct("2023-11-14 03:15:52 MST"), tz = "MST")-10
ie <- as.integer(as.POSIXct("2023-11-14 03:42:03 MST"), tz = "MST")+10

cs <- as.integer(as.POSIXct("2023-11-14 02:44:35 MST"), tz = "MST")-10
ce <- as.integer(as.POSIXct("2023-11-14 03:15:01 MST"), tz = "MST")+10

ins <- as.integer(as.POSIXct("2023-11-14 02:26:55 MST"), tz = "MST")-10
ine <- as.integer(as.POSIXct("2023-11-14 02:30:45 MST"), tz = "MST")+10

o <- d[d$time >= os & d$time <= oe,]
c <- d[d$time >= cs & d$time <= ce,]
i <- d[d$time >= is & d$time <= ie,]

o$time = o$time - min(o$time)
c$time = c$time - min(c$time)
i$time = i$time - min(i$time)


ggplot() +
    geom_line(o,mapping=aes(x=time, y=active), color=o_color, linewidth=0.75) +
    scale_y_continuous(breaks=seq(1500, 4000, 200)) +
    ylab("Power Draw [W]") +
    xlab("Time [s]") +
    ggtitle("Power Draw vs Time (Open MPI)")
ggsave("power_ompi.png", width=200, height=75,units = "mm")

ggplot() +
    geom_line(c,mapping=aes(x=time, y=active), color=c_color, linewidth=0.75) +
    scale_y_continuous(breaks=seq(1500, 4000, 200)) +
    ylab("Power Draw [W]") +
    xlab("Time [s]") +
    ggtitle("Power Draw vs Time (MPICH)")
ggsave("power_ch.png", width=200, height=75,units = "mm")

ggplot() +
    geom_line(i,mapping=aes(x=time, y=active), color=i_color, linewidth=0.75) +
    scale_y_continuous(breaks=seq(1500, 4000, 200)) +
    ylab("Power Draw [W]") +
    xlab("Time [s]") +
    ggtitle("Power Draw vs Time (Intel MPI)")
ggsave("power_intel.png", width=200, height=75,units = "mm")
    

s <- d[d$time >= ins & d$time <= ine,]
s$time = s$time - min(s$time)
ggplot() +
    geom_line(s, mapping=aes(x=time, y=active), color=o_color, linewidth=0.75) +
    scale_y_continuous(breaks=seq(1500, 4000, 200)) +
    ylab("Power Draw [W]") +
    xlab("Time [s]") +
    ggtitle("Power Draw vs Time (Individual Run)")
ggsave("power_individual.png", width=100, height=65,units = "mm")