library(ggplot2)
library(ggthemes)
theme_set(theme_bw(base_size=10))

o_color <- "#619cff"
c_color <- "#00ba38"
i_color <- "#f8766d"


o <- read.table("power_consumption_ompi.log", sep="", header=TRUE)
c <- read.table("power_consumption_mpich.log", sep="", header=TRUE)
i <- read.table("power_consumption_intel.log", sep="", header=TRUE)
o$time = o$time - min(o$time)
c$time = c$time - min(c$time)
i$time = i$time - min(i$time)


ggplot() +
    geom_line(o,mapping=aes(x=time, y=active), color=o_color) +
    scale_y_continuous(breaks=seq(1500, 4000, 200)) +
    ylab("Power Draw [W]") +
    xlab("Time [s]") +
    ggtitle("Power Draw vs Time (Open MPI)")
ggsave("power_ompi.png", width=200, height=75,units = "mm")

ggplot() +
    geom_line(c,mapping=aes(x=time, y=active), color=c_color) +
    scale_y_continuous(breaks=seq(1500, 4000, 200)) +
    ylab("Power Draw [W]") +
    xlab("Time [s]") +
    ggtitle("Power Draw vs Time (MPICH)")
ggsave("power_ch.png", width=200, height=75,units = "mm")

ggplot() +
    geom_line(i,mapping=aes(x=time, y=active), color=i_color) +
    scale_y_continuous(breaks=seq(1500, 4000, 200)) +
    ylab("Power Draw [W]") +
    xlab("Time [s]") +
    ggtitle("Power Draw vs Time (Intel MPI)")
ggsave("power_intel.png", width=200, height=75,units = "mm")
    

s <- read.table("cha_11445_power.log", sep="", header=TRUE)
s$time = s$time - min(s$time)
ggplot() +
    geom_line(s, mapping=aes(x=time, y=active), color=o_color) +
    scale_y_continuous(breaks=seq(1500, 4000, 200)) +
    ylab("Power Draw [W]") +
    xlab("Time [s]") +
    ggtitle("Power Draw vs Time (Individual Run)")
ggsave("power_individual.png", width=100, height=65,units = "mm")
