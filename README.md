# scc23_reproducibility_racklette

This is the official repository of team RACKlette for the SCC23 reproducibility challenge. It hosts all the scrips used to run the application and to analyze and plot the gathered data. It is split into three parts, the compiling part, the experiment part and the evaluation part. In the compiling part are all the `spack` files with which we compiled the application. In the experiment part are all the `bash` and `sbatch` scripts used to run the application. And in the evaluation part are the scripts used to evaluate and plot the data.
