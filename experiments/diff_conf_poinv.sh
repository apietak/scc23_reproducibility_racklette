#!/bin/bash
# This script test different tile sizes
TILE=500
NS=(25 50 75 100 125 150 175 200)
REP=4
# export matrix size
export B="$TILE"
export P=""
export T="-t 63"
export CUST=""
# All the slurm sbatch parameters
NODES=4
TASKS=8
CPUS_PER_TASK=64
TASKS_PER_NODE=2

# As we cannot read a value in a comment we cannot directly set the sbatch
# parameters, therefore we copy the run script and sed the copy
cp m_run_poinv.sh m_run_tmp.sh
replace_params () {
        sed -i "s/NODES/$NODES/g" m_run_tmp.sh
        sed -i "s/NTASKS/$TASKS/g" m_run_tmp.sh
        sed -i "s/CPUS_PER_TASK/$CPUS_PER_TASK/g" m_run_tmp.sh
        sed -i "s/TASKS_PER_NODE/$TASKS_PER_NODE/g" m_run_tmp.sh
        sed -i "s+JOB_OUTPUT+$JOB_OUTPUT+g" m_run_tmp.sh
}



DATE=$(date '+%Y%m%d-%H%M%S')
FOLDER="poinv/""$1/""$DATE"
mkdir -p "$FOLDER"
JOB_OUTPUT_FILE="cha_"
JOB_OUTPUT="$FOLDER/$JOB_OUTPUT_FILE"


replace_params

if true
then


for N_ in ${NS[@]}
do
        for ((i=1; i <=$REP; i++))
        do
                export N="-n $((N_ * TILE))"
                sbatch m_run_tmp.sh
        done
done

fi


export CUST="--custom=/home/apietak/sc23/rep/sbc.txt"
cp m_run_poinv.sh m_run_tmp.sh
JOB_OUTPUT_FILE="cha_sbc_"
JOB_OUTPUT="$FOLDER/$JOB_OUTPUT_FILE"
replace_params

if true
then
for N_ in ${NS[@]}
do
        for ((i=1; i<=$REP; i++))
        do
                export N="-n $((N_ * TILE))"
                sbatch m_run_tmp.sh
        done
done

fi