#!/bin/bash
#SBATCH --job-name=chameleon # Job name
#SBATCH --ntasks=NTASKS           # Number of MPI tasks (i.e. processes)
#SBATCH --cpus-per-task=CPUS_PER_TASK            # Number of cores per MPI task
#SBATCH --distribution=cyclic:cyclic # Distribute tasks cyclically first among nodes and then among sockets within a node
#SBATCH --exclusive
#SBATCH --time=01:00:00              # Wall time limit (days-hrs:min:sec)
#SBATCH --output=JOB_OUTPUT%j     # Path to the standard output and error files relative to the working directory
#SBATCH --cpu-freq=1800

echo "Date              = $(date)"
echo "Hostname          = $(hostname -s)"
echo "Working Directory = $(pwd)"
echo "chameleon         = $(which chameleon_dtesting)"
echo ""
echo "Running with the following script: "
echo "-----------------------------------"
cat $BASH_SOURCE
echo "-----------------------------------"
echo ""
echo ""

#export STARPU_WORKERS_GETBIND=0
export STARPU_MPI_STATS=1

source $MPI

sleep 5
srun --mpi=pmi2 --cpu-bind=sockets chameleon_dtesting -H -o poinv $T $N -b $B $CUST --mtxfmt=1
sleep 5
echo "Date             = $(date)"