#!/bin/bash
# This script test different tile sizes
TILES=(100 200 300 400 500 700 900 1000)
REP=5

# export matrix size
export N="-n 50000"
export P=""
export T="-t 63"
export CUST=""
# All the slurm sbatch parameters
NODES=1
TASKS=1
CPUS_PER_TASK=64
TASKS_PER_NODE=1

# As we cannot read a value in a comment we cannot directly set the sbatch
# parameters, therefore we copy the run script and sed the copy
cp m_run.sh m_run_tmp.sh
replace_params () {
        sed -i "s/NODES/$NODES/g" m_run_tmp.sh
        sed -i "s/NTASKS/$TASKS/g" m_run_tmp.sh
        sed -i "s/CPUS_PER_TASK/$CPUS_PER_TASK/g" m_run_tmp.sh
        sed -i "s/TASKS_PER_NODE/$TASKS_PER_NODE/g" m_run_tmp.sh
        sed -i "s+JOB_OUTPUT+$JOB_OUTPUT+g" m_run_tmp.sh
}



DATE=$(date '+%Y%m%d-%H%M%S')
FOLDER="tiles/"$DATE
mkdir -p "$FOLDER"
JOB_OUTPUT_FILE="cha_tiles_"
JOB_OUTPUT="tiles/"$DATE"/$JOB_OUTPUT_FILE"


replace_params


for TILE in ${TILES[@]}
do
        for ((i=1; i<=$REP; i++))
        do
                export B=$TILE
                sbatch m_run_tmp.sh
        done
done